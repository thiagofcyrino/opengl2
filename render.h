#ifndef UNI_RENDER_H_
#define UNI_RENDER_H_

#include "material.h"
#include "shader.h"

namespace uni {

class Render {
public:
  enum RenderModeE {TRIANGLE_MODE, LINE_MODE, POINT_MODE, RENDER_MODE_SIZE};
  enum DataTypeE {POSITION_TYPE = 0, NORMAL_TYPE, TEXTURE_COORD_TYPE, DATA_TYPE_SIZE};
public:
  Render(bool useShader = false);
  virtual ~Render();

  void setMaterial(const Material& mat);
  void setShader(Shader* shader);
  void setUseShader(bool status);

  virtual void draw();

protected:
  void initIndices(unsigned *indices, unsigned indicesSize);
  void initBuffer(float* data, unsigned dataSize, DataTypeE type);
  void drawVBO();

protected:
  unsigned _indicesSize;
  RenderModeE _renderMode;
  unsigned _data[DATA_TYPE_SIZE]; //pos, ind, norm, coord ...
  unsigned _indiceVB;
  Material _material;
  Shader* _shader;
  bool _useShader;

private:
    static const unsigned _renderModes[3];
};

} //END namespace

#endif
