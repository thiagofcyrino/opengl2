#include "texture.h"

namespace uni {

Texture::Texture()
{

}

Texture::~Texture()
{

}

Texture::Texture(unsigned int index, unsigned int width, unsigned int height)
{
  _height = height;
  _width = width;
  _index = index;
}
unsigned int Texture::height() const
{
  return _height;
}
unsigned int Texture::index() const
{
  return _index;
}
unsigned int Texture::width() const
{
  return _width;
}


}