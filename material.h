#ifndef UNI_MATERIAL_H_
#define UNI_MATERIAL_H_

#include "color.h"

namespace uni {

class Material {
public:
    Material();
    virtual ~Material();

    void setAmbient(const Color& c);
    void setDifuse(const Color& c);
    void setSpecular(const Color& c);
    void setShininess(float v);

    virtual void setMaterial();

private:
    Color _a, _d, _s;
    float _shininess;
};

} //END namespace

#endif
