#ifndef UNI_INIT_MANAGER_H_
#define UNI_INIT_MANAGER_H_

namespace uni {
  
class InitManager {
public:
  virtual ~InitManager();
  
  static void initSDLVideo(unsigned w, unsigned h);
  static void initGL();
  static void initView(unsigned w, unsigned h);
  static void initGlew();
  
private:
  InitManager();
};
  
} //END namespace

#endif
