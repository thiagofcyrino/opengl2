#ifndef UNI_SG_H_
#define UNI_SG_H_

#include <SDL/SDL.h>
#include "render.h"
#include "light.h"
#include "triangle_mesh_render.h"
#include "shader.h"
#include "texture.h"

namespace uni {
  
class SG {
public:
  SG(unsigned w, unsigned h);
  virtual ~SG();
  
private:
  void start();
  void init();
  void update();
  void mainLoop();
  void draw();
  void modify();
  void checkEvent(SDL_Event& e);
  void initShader();
  void modifyShader();
  
  
  void initCubeVBO();
  void drawVBO(unsigned int mode, unsigned int sizeVertex);

private:
  unsigned _w, _h;
  bool _done;
  
  float _rotation, _rX, _rY, _coneAngle, _coneX, _coneY;

  TriangleMeshRender* _render;
  TriangleMesh _mesh;
  Light* _light;
  Shader* _shader;
  bool _useShader;
  
  
  Texture _tex; 
  unsigned int _indexVBO;
  unsigned int _positionVBO;
  unsigned int _colorVBO;
  unsigned int _textureVBO;
};
  
} //END namespace

#endif
