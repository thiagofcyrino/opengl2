 
#include "triangle_mesh.h"

#include <iostream>

namespace uni {

TriangleMesh::TriangleMesh()
  :_maxFaceIndice(0)
{}
TriangleMesh::~TriangleMesh() {}

unsigned int TriangleMesh::amountOfVertex() const {
  return _vertex.size();
}

void TriangleMesh::addVertex(const Vertex& v) {
  _vertex.push_back(v);
}

Vertex TriangleMesh::vertex(unsigned int idx) const {
  return _vertex[idx];
}

void TriangleMesh::addNormal(const Vertex& n) {
  _normal.push_back(n);
}

Vertex TriangleMesh::normal(unsigned int idx) const {
  return _normal[idx];
}

unsigned int TriangleMesh::amountOfFace() const {
   return _face.size();
}

void TriangleMesh::addFace(const Face& f) {
  for (unsigned i = 0; i < 3; ++i) {
     _maxFaceIndice = std::max(_maxFaceIndice, f.idx(i));
  }

  _face.push_back(f);
}

Face TriangleMesh::face(unsigned int idx) const {
   return _face[idx];
}

unsigned TriangleMesh::getMaxFaceIndice() const {
   return _maxFaceIndice;
}

} //END namespace
