#ifndef UNI_VERTEX_H_
#define UNI_VERTEX_H_

namespace uni {
	
class Vertex {
public:	
  Vertex(float x = 0.f, float y = 0.f, float z = 0.f) ;

  virtual ~Vertex();

  void x(float x);
  void y(float y);
  void z(float z);

  float x() const;
  float y() const;
  float z() const;

  Vertex operator*(float scalar) const;
  Vertex operator+(const Vertex& other) const;
  Vertex operator-(const Vertex& other) const;
  Vertex cross(const Vertex& other) const;
  Vertex normalize() const;

  void print() const;
	
private:
  float _x, _y, _z;
};	

} //END namespace

#endif
