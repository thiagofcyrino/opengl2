#include "loader_factory.h"

#include <iostream>

#include <fstream>
#include <sstream>
#include <cstdio>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>


#include <GL/glew.h>
#include <assert.h>

namespace uni {

LoaderFactory::LoaderFactory() {}
LoaderFactory::~LoaderFactory() {}

TriangleMesh LoaderFactory::createMeshFromObjFile(const std::string& path) {
  TriangleMesh mesh;

  std::ifstream fileLoad;
  fileLoad.open(path.c_str());

  std::string line, tmp;

  float x, y, z;
  int va, na, vb, nb, vc, nc;

  while(std::getline(fileLoad, line)) {
    std::stringstream st(line);

    switch(line[0]) {
      case 'v':
      switch(line[1]) {
        case ' ': //vertex
          st >> tmp >> x >> y >> z;
          mesh.addVertex(Vertex(x, y, z));
          break;
        case 'n': //normal
          st >> tmp >> x >> y >> z;
          mesh.addNormal(Vertex(x, y, z));
          break;
      }
      break;

      case 'f':
        sscanf(line.c_str(), "f %d//%d %d//%d %d//%d", &va, &na, &vb, &nb, &vc, &nc);
        mesh.addFace(Face(va - 1, vb - 1, vc - 1, na - 1, nb - 1, nc -1));
        break;

      default: break;
    }
  }
  fileLoad.close();

//     std::cout << "INIT ParcerObj" << std::endl;
// 
//     char temp_char;
//     float x, y, z;
//     unsigned int v1,v2,v3,vn1, vn2, vn3;
//     std::string temp_string;
//     std::stringstream ss;
//     std::fstream file;
// 
//     file.open(path.c_str(), std::ios::in);
//     if (file.is_open())
//         std::cout << "Arquivo aberto" << std::endl;
//     else
//         std::cout << "erro ao abrir arquivo" << std::endl;
//     unsigned count = 0;
// 
// 
//     std::vector<Vertex> pos;
//     std::vector<Vertex> normal;
// 
//     while (std::getline(file,temp_string)) {
//         ss.str(std::string());
//         ss.clear();
//         ss << temp_string;
// 
//         if (temp_string[0] == 'v' && temp_string[1] == 'n') {
//             ss >> temp_char >> temp_char >> x >> y >> z;
//             Vertex n(x,y,z);
//             normal.push_back(n);
//         }
// 
//         if (temp_string[0] == 'v' && temp_string[1] == ' ') {
//             ss >> temp_char >> x >> y >> z;
//             Vertex p(x,y,z);
//             pos.push_back(p);
//         }
//         else {
//             if (temp_string[0] == 'f' && temp_string[1] == ' ') {
//                 ss >> temp_char >> v1 >> temp_char >> temp_char >> vn1
//                 >> v2 >> temp_char >> temp_char >> vn2
//                 >> v3 >> temp_char >> temp_char >> vn3;
// 
// 		mesh.addNormal(normal.at(vn1-1));		
// 		mesh.addNormal(normal.at(vn2-1));
// 		mesh.addNormal(normal.at(vn3-1));
// 		mesh.addVertex(pos.at(v1-1));
// 		mesh.addVertex(pos.at(v2-1));
// 		mesh.addVertex(pos.at(v3-1));
// 		
// 		mesh.addFace(Face(v1 - 1, v2 - 1, v3 - 1, vn1 - 1, vn2 - 1, vn3 -1));
//             }
//         }
//     }
// 
//     file.close();

  return mesh;
}

std::string LoaderFactory::loadShaderCode(const std::string &path) {
   std::ifstream file(path.c_str());
  
  
  if(file.is_open())
    std::cout << "Arquivo aberto" << std::endl;
  else
      std::cout << "erro ao abrir arquivo" << std::endl;
  std::string str;

  file.seekg(0, std::ios::end);   
  str.reserve(file.tellg());
  file.seekg(0, std::ios::beg);

  str.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
  
  return str;
}

Texture LoaderFactory::loadTextureImage(const std::string& path)
{
 
  SDL_Surface* surface = IMG_Load(path.c_str()); 
 
  assert (surface && "imagem não encontrada");   
  
  unsigned w = surface->w;
  unsigned h = surface->h;
  
  unsigned components = surface->format->BitsPerPixel;
  unsigned textId;
  glGenTextures(1,&textId);
  
  Texture tex(textId,w,h);
  glActiveTexture(GL_TEXTURE0);
  
  glBindTexture(GL_TEXTURE_2D , textId);
  
  int glMode = GL_RGB;
  
  if(components ==4)
      glMode = GL_RGBA;
        
  glTexImage2D(GL_TEXTURE_2D , 0 , components , w, h, 0, glMode , GL_UNSIGNED_BYTE , surface->pixels);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  
  
  glBindTexture(GL_TEXTURE_2D,0);
  
  
  SDL_FreeSurface(surface);
  
  return tex;
    
}


} //END namespace
