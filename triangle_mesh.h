#ifndef UNI_MESH_H_
#define UNI_MESH_H_

#include <vector>
#include "face.h"
#include "vertex.h"

namespace uni {

class TriangleMesh {
public:
  TriangleMesh();
  virtual ~TriangleMesh();

  unsigned int amountOfVertex() const;
  void addVertex(const Vertex& v);
  Vertex vertex(unsigned int idx) const;

  void addNormal(const Vertex& n);
  Vertex normal(unsigned int idx) const;

  unsigned int amountOfFace() const;
  void addFace(const Face& f);
  Face face(unsigned int idx) const;

  unsigned getMaxFaceIndice() const;

private:
    std::vector<Vertex> _vertex;
    std::vector<Face> _face;
    std::vector<Vertex> _normal;
    unsigned _maxFaceIndice;
};

} //END namespace

#endif

