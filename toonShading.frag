varying vec3 N;
varying vec3 v;
    

uniform vec4 light_p; 
uniform vec4 light_ks; 
uniform vec4 light_kd; 
uniform vec4 light_a;   

uniform float mat_sh;

varying vec3 cor;
void main(){
   vec3 L = normalize(light_p.xyz - v);   
   vec3 E = normalize(-v);
   vec3 R = normalize(-reflect(L,N));  


   //calculate Ambient Term:  
   vec4 Iamb = light_a;    

   //calculate Diffuse Term:  
   vec4 Idiff = light_kd * max(dot(N,L), 0.0);
   Idiff = clamp(Idiff, 0.0, 1.0);     
   
   // calculate Specular Term:
   vec4 Ispec = light_ks * pow(max(dot(R,E),0.0),0.3*mat_sh);
   Ispec = clamp(Ispec, 0.0, 1.0); 

   // write Total Color:  
//    gl_FragColor =  Iamb + Idiff + Ispec;    

  float intensity;
  vec4 color = Iamb + Idiff + Ispec;
  intensity = dot(L,N);

  if (intensity > 0.95)
    color = vec4(1.0,0.5,0.5,1.0);
  else if (intensity > 0.5)
    color = vec4(0.6,0.3,0.3,1.0);
  else if (intensity > 0.25)
    color = vec4(0.4,0.2,0.2,1.0);
  else
    color = vec4(0.2,0.1,0.1,1.0);
  gl_FragColor = color;


}