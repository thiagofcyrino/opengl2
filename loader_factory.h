#ifndef UNI_FACTORY_LOADER_H_
#define UNI_FACTORY_LOADER_H_

#include <string>
#include "triangle_mesh.h"
#include "texture.h"

namespace uni {

class LoaderFactory {
public:
    virtual ~LoaderFactory();

    static TriangleMesh createMeshFromObjFile(const std::string& path);
    static std::string loadShaderCode(const std::string& path);
    static Texture loadTextureImage(const std::string& path);

private:
    LoaderFactory();
};
	
} //END namespace

#endif
