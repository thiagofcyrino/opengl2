varying vec3 N;
varying vec3 v;


uniform vec4 light_p; 
uniform vec4 light_ks; 
uniform vec4 light_a;   
uniform vec4 light_kd; 

uniform float mat_sh;

uniform vec3 spot_direction;
uniform float spot_cutoff;

varying vec3 cor;
void main(){
  vec3 L = normalize(light_p.xyz - v);   
  vec3 E = normalize(-v);
  vec3 R = normalize(-reflect(L,N));  

  //calculate Ambient Term:  
  vec4 Iamb = light_a;   

  //point light       
  float distanceToLight = length(L);

  //cone restrictions (affects attenuation)
  float lightToSurfaceAngle = degrees(acos(dot(-L, normalize(spot_direction))));

  if( lightToSurfaceAngle < spot_cutoff){

    //calculate Diffuse Term:  
    vec4 Idiff = light_kd * max(dot(N,L), 0.0);
    Idiff = clamp(Idiff, 0.0, 1.0);     

    // calculate Specular Term:
    vec4 Ispec = light_ks * pow(max(dot(R,E),0.0),0.3*mat_sh);
    Ispec = clamp(Ispec, 0.0, 1.0); 

    // write Total Color:  
    gl_FragColor =  Iamb + Idiff + Ispec;    
  }else if (lightToSurfaceAngle == spot_cutoff){
    gl_FragColor =  vec4(0,0,1,1)  ;    
  }
  else{
    gl_FragColor =  Iamb  ;    
  }

}