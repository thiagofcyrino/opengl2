#ifndef UNI_SHADER_H_
#define UNI_SHADER_H_

#include <string>
#include <map>
#include <tr1/tuple>

namespace uni {

class Shader {
public:
  Shader(bool showLog = false);
  virtual ~Shader();

  void vertex(const std::string& code);
  void fragment(const std::string& code);
  void build();
  void enable();
  void disable();

  void setUniform(float v0, const std::string& name);
  void setUniform(float v0, float v1, const std::string& name);
  void setUniform(float v0, float v1, float v2, const std::string& name);
  void setUniform(float v0, float v1, float v2, float v3, const std::string& name);

  void showLog(bool status);

private:
  void shaderCode(const std::string& code, unsigned idx);
  void log();
  std::string shaderLog(unsigned idx);
  std::string programLog(unsigned idx);

private:
  typedef std::map<std::string, float> MapT1;
  typedef std::map<std::string, std::tr1::tuple<float, float> > MapT2;
  typedef std::map<std::string, std::tr1::tuple<float, float, float> > MapT3;
  typedef std::map<std::string, std::tr1::tuple<float, float, float, float> > MapT4;


  unsigned _vertexSh, _fragmentSh, _program;
  bool _showLog;

  MapT1 _uniform1;
  MapT2 _uniform2;
  MapT3 _uniform3;
  MapT4 _uniform4;

};

} //END namespace

#endif
