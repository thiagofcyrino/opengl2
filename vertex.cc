#include "vertex.h"

#include <cmath>
#include <iostream>

namespace uni {
	
Vertex::Vertex(float x, float y, float z) 
:_x(x), _y(y), _z(z)
{
}

Vertex::~Vertex() {}

void Vertex::x(float x) { _x = x; }
void Vertex::y(float y) { _y = y; }
void Vertex::z(float z) { _z = z; }

float Vertex::x() const { return _x; }
float Vertex::y() const { return _y; }
float Vertex::z() const { return _z; }

Vertex Vertex::operator*(float scalar) const {
    return Vertex(_x * scalar, _y * scalar, _z * scalar);
}

Vertex Vertex::operator+(const Vertex &other) const {
  return Vertex(
              _x + other.x(),
              _y + other.y(),
              _z + other.z()
              );
}

Vertex Vertex::operator-(const Vertex &other) const {
  return Vertex(
              _x - other.x(),
              _y - other.y(),
              _z - other.z()
              );
}

Vertex Vertex::cross(const Vertex &other) const {
  return Vertex(
              _y * other.z() - _z * other.y(),
              _z * other.x() - _x * other.z(),
              _x * other.y() - _y * other.x()
              );
}

Vertex Vertex::normalize() const {
  float module = fabs(sqrt(_x * _x + _y * _y + _z * _z));
  return Vertex(
                  _x / module,
                  _y / module,
                  _z / module
              );
}

void Vertex::print() const {
  std::cout << "point: " <<  _x << ", " << _y << ", " << _z << std::endl;
}
	
} //END namespace
