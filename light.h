#ifndef UNI_LIGHT_H_
#define UNI_LIGHT_H_

#include "vertex.h"
#include "color.h"

namespace uni {

class Light {
public:
    Light(const Vertex& pos, unsigned idx = 0);
    virtual ~Light();

    void setAmbient(const Color& c);
    void setDiffuse(const Color& c);
    void setSpecular(const Color& c);

    void enable(bool status);

private:
    unsigned _idx;
};

} //END namespace

#endif
