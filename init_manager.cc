#include "init_manager.h"

#include <SDL/SDL.h>
#include <GL/glew.h>
#include <iostream>

namespace uni {
  
InitManager::~InitManager() {}

void InitManager::initSDLVideo(unsigned w, unsigned h) {
  SDL_Init(SDL_INIT_EVERYTHING);

  int flags 	 =	SDL_OPENGL;
  flags	    	|=	SDL_GL_DOUBLEBUFFER;
  flags	    	|=	SDL_HWPALETTE;
  flags	    	|=	SDL_RESIZABLE;
  flags	    	|=	SDL_SWSURFACE;

  const SDL_VideoInfo *videoInfo = SDL_GetVideoInfo();

  if (!videoInfo) {
      ::exit(1);
  }

  if (videoInfo->hw_available) 	flags |= SDL_HWSURFACE;
  else 				flags |= SDL_SWSURFACE;

  if (videoInfo->blit_hw) 	flags |= SDL_HWACCEL;


  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  static const int bits = 16;
  SDL_SetVideoMode(w, h, bits, flags);

  static const int delay = 10;
  static const int interval = 100;
  SDL_EnableKeyRepeat(delay, interval);
  
}

void InitManager::initGL() {
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glDepthFunc(GL_LEQUAL);
  glShadeModel(GL_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  //habilitar aqui a luz geral
}

void InitManager::initView(unsigned w, unsigned h) {
  float ratio = (float) w / (float) h;
  glViewport(0, 0, w, h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  static const float angle = 45.f;
  static const float nearPlane = .1f;
  static const float farPlane = 100.f;
  gluPerspective( angle, ratio, nearPlane, farPlane);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void InitManager::initGlew() {
  GLenum err = glewInit();
  if (GLEW_OK != err) 		std::cout << "glew erro! =[" << std::endl;
  else 				std::cout << "glew working. =]" << std::endl;
}
  
InitManager::InitManager() {}

} //END namespace
