#include "shader.h"
#include <GL/glew.h>
#include <iostream>

namespace uni {

Shader::Shader(bool showLog)
  :_showLog(showLog)
{
  _program = glCreateProgram();
  _fragmentSh = glCreateShader(GL_FRAGMENT_SHADER);  
  _vertexSh = glCreateShader(GL_VERTEX_SHADER);
}

Shader::~Shader() {
    glDetachShader(_program,_vertexSh);
    glDeleteShader(_vertexSh);

    glDetachShader(_program,_fragmentSh);
    glDeleteShader(_fragmentSh);

    glDeleteProgram(_program);
}

void Shader::vertex(const std::string &code) {
  this->shaderCode(code, _vertexSh);
}

void Shader::fragment(const std::string &code) {
  this->shaderCode(code, _fragmentSh);
}

void Shader::build() {
  glAttachShader(_program, _vertexSh);
  glAttachShader(_program, _fragmentSh);

  glLinkProgram(_program);

  if (_showLog) this->log();
}

void Shader::enable() {
  glUseProgram(_program);
  /*
  for (MapT1::iterator it=_uniform1.begin(); it!=_uniform1.end(); ++it){
    const char* name = it->first.c_str();
    GLint loc = glGetUniformLocation(_program, name);
    glUniform1f(loc, it->second );
  }
  for (MapT2::iterator it=_uniform2.begin(); it!=_uniform2.end(); ++it){
    const char* name = it->first.c_str();
    GLint loc = glGetUniformLocation(_program, name);
  }
  for (MapT3::iterator it=_uniform3.begin(); it!=_uniform3.end(); ++it){
    const char* name = it->first.c_str();
    GLint loc = glGetUniformLocation(_program, name);
  }
  for (MapT4::iterator it=_uniform4.begin(); it!=_uniform4.end(); ++it){
    const char* name = it->first.c_str();
    GLint loc = glGetUniformLocation(_program, name);
    
  }*/

}

void Shader::disable() {
  glUseProgram(0);
}

void Shader::setUniform(float v0, const std::string &name) {
  _uniform1[name] = v0;
  const char* nameChar = name.c_str();
  GLint loc = glGetUniformLocation(_program, nameChar);
  glUniform1f(loc, v0 );
}

void Shader::setUniform(float v0, float v1, const std::string &name) {
  _uniform2[name] = std::tr1::make_tuple<float, float>(v0, v1);
  const char* nameChar = name.c_str();
  GLint loc = glGetUniformLocation(_program, nameChar);
  glUniform2f(loc, v0, v1 );
}

void Shader::setUniform(float v0, float v1, float v2, const std::string &name) {
  _uniform3[name] = std::tr1::make_tuple<float, float, float>(v0, v1, v2);
  const char* nameChar = name.c_str();
  GLint loc = glGetUniformLocation(_program, nameChar);
  glUniform3f(loc, v0,v1,v2 );
}

void Shader::setUniform(float v0, float v1, float v2, float v3, const std::string &name) {
  const char* nameChar = name.c_str();
  GLint loc = glGetUniformLocation(_program, nameChar);
  glUniform4f(loc, v0,v1,v2,v3 );
  _uniform4[name] = std::tr1::make_tuple<float, float, float, float>(v0, v1, v2, v3);
//   std::cout << "uniform"<< name << v0  <<  "  " << v1  <<  "  " << v2  <<  "  " << v3  <<  "  " << std::endl;
}

void Shader::showLog(bool status) {
  _showLog = status;
}

void Shader::shaderCode(const std::string &code, unsigned idx) {
  const char* shaderChar = code.c_str();

    glShaderSource(idx, 1, &shaderChar, 0);
    glCompileShader(idx);

}

void Shader::log() {
  std::string log = this->shaderLog(_vertexSh);
  log += this->shaderLog(_fragmentSh);
  log += this->programLog(_program);
  std::cout << "###########################" << std::endl;
  std::cout << log << std::endl;
  std::cout << "###########################" << std::endl;
}

std::string Shader::shaderLog(unsigned idx) {
  int infologLength = 0;
  int charsWritten  = 0;
  char *infoLog;
  std::string log = "";

  glGetShaderiv(idx, GL_INFO_LOG_LENGTH, &infologLength);

  if (infologLength > 0) {
    infoLog = new char[infologLength];
    glGetShaderInfoLog(idx, infologLength, &charsWritten, infoLog);

    std::string str(infoLog);

    if (!str.empty()) {
      log += "\n" + str;
    } else {
      log += "Done.\n";
    }

    delete [] infoLog;
  }
  return log;
}

std::string Shader::programLog(unsigned idx) {
  int infologLength = 0;
  int charsWritten  = 0;
  char *infoLog;
  std::string log = "";

  glGetProgramiv(idx, GL_INFO_LOG_LENGTH, &infologLength);

  if (infologLength > 0) {
    infoLog = new char[infologLength];
    glGetProgramInfoLog(idx, infologLength, &charsWritten, infoLog);

    std::string str(infoLog);

    if (!str.empty()) {
      log += "\n" + str;
    } else {
      log += "Done.\n";
    }

    delete [] infoLog;
  }
  return log;
}


} //END namespace
