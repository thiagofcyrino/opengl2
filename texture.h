#ifndef TEXTURE_H
#define TEXTURE_H

namespace uni {
  
class Texture
{

public:
Texture();
Texture(unsigned index,unsigned width,unsigned height);
virtual ~Texture();

unsigned index() const;
unsigned width() const;
unsigned height() const;

private:
unsigned _index, _width, _height;

};

}
#endif // TEXTURE_H
