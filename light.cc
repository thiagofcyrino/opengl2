#include "light.h"
#include <GL/glew.h>

namespace uni {

Light::Light(const Vertex& pos, unsigned idx) {
  _idx = idx;

  float cArray[] = {pos.x(), pos.y(), pos.z(), 0.0};
    //informa posicao da luz
  
  glLightfv(_idx,GL_POSITION,cArray);
  
  
}

Light::~Light() {}

void Light::setAmbient(const Color &c) {
  float cArray[] = {c._r, c._g, c._b, c._a};
    //informa ambiente da luz
  glMaterialfv(GL_FRONT,GL_AMBIENT,cArray);
}

void Light::setDiffuse(const Color &c) {
  float cArray[] = {c._r, c._g, c._b, c._a};
   //informa diffusa da luz
  glMaterialfv(GL_FRONT,GL_DIFFUSE,cArray);
}

void Light::setSpecular(const Color &c) {
  float cArray[] = {c._r, c._g, c._b, c._a};  
  glMaterialfv(GL_FRONT,GL_SPECULAR,cArray);
}

void Light::enable(bool status) {
  glEnable(GL_LIGHTING);
  glEnable(_idx);
  glEnable(GL_DEPTH_TEST);
}

}//END namespace
