#ifndef UNI_FACE_H_
#define UNI_FACE_H_

namespace uni {

class Face {
public:
  Face();
	
  Face(unsigned int idx0, unsigned int idx1, unsigned int idx2,
       unsigned int inx0 = 0, unsigned int inx1 = 0, unsigned int inx2 = 0
       );
  virtual ~Face();
	
  unsigned int idx(unsigned int i) const; //MAX 3
  unsigned int inx(unsigned int i) const; //MAX 3
	
private:
    unsigned int _idx[3];
    unsigned int _inx[3];
};
	
} //END namespace

#endif
