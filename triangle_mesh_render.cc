#include "triangle_mesh_render.h"

#include <GL/glew.h>
#include <cstring>
#include <iostream>

namespace uni {

TriangleMeshRender::TriangleMeshRender(const TriangleMesh & tMesh, bool calculateNormals)
    :Render()
{
  
 this->_calculateNormals = calculateNormals;
 this->init(tMesh);
}

 
TriangleMeshRender::~TriangleMeshRender() {}

void TriangleMeshRender::init(const TriangleMesh& mesh) {
  this->setPosition(mesh);
  this->setIndices(mesh);
  this->setNormal(mesh);
}

void TriangleMeshRender::setPosition(const TriangleMesh &mesh) {
  unsigned verticeSize = mesh.amountOfVertex() * 3;  // vertex * x,y,z

  float *verticesPosition = new float [verticeSize];
  for (unsigned i = 0; i < mesh.amountOfVertex(); ++i) {
    verticesPosition[i * 3 + 0] = mesh.vertex(i).x();
    verticesPosition[i * 3 + 1] = mesh.vertex(i).y();
    verticesPosition[i * 3 + 2] = mesh.vertex(i).z();
  }
  this->initBuffer(verticesPosition, verticeSize, Render::POSITION_TYPE);
  delete [] verticesPosition;
}

void TriangleMeshRender::setIndices(const TriangleMesh &mesh) {
  _indicesSize = mesh.amountOfFace() * 3;
  unsigned* indices = new unsigned[_indicesSize];
  for (unsigned i = 0; i < mesh.amountOfFace(); ++i) {
    indices[i * 3 + 0] = mesh.face(i).idx(0);
    indices[i * 3 + 1] = mesh.face(i).idx(1);
    indices[i * 3 + 2] = mesh.face(i).idx(2);
  }
  this->initIndices(indices, _indicesSize);
  delete [] indices;
}

void TriangleMeshRender::setNormal(const TriangleMesh &mesh) {
  if(_calculateNormals){
    
    unsigned normalSize = mesh.amountOfVertex() * 3;  
    float *normalPosition = new float [normalSize];
    
    std::vector<Face> f;
    std::vector<Vertex> fn;
    Vertex finalNormal(.0,.0,.0);
    finalNormal.print();
    for(int i=0; i < mesh.amountOfVertex(); i++ ){
      f.clear();
      fn.clear();
      for(int j=0; j < mesh.amountOfFace(); j++){
	if(mesh.face(j).idx(0) == i || mesh.face(j).idx(1) == i || mesh.face(j).idx(2) == i){
	  f.push_back(mesh.face(j));
	}
      }
      for(int w=0; w < f.size(); w++){
	Vertex a = mesh.normal(f.at(w).inx(0)) ;
	Vertex b = mesh.normal(f.at(w).inx(1)) ;
	Vertex c = mesh.normal(f.at(w).inx(2)) ;

	Vertex n1 = b - a;
	Vertex n2 = c - a;

	Vertex face_normal = n1.cross(n2);
	fn.push_back(face_normal);
      }
      for(int y=0; y< fn.size(); y++){
	finalNormal = finalNormal  + fn.at(y);
      }

      finalNormal = finalNormal.normalize();
      
      normalPosition [i * 3 ] = finalNormal.x();
      normalPosition [i * 3 + 1] = finalNormal.y();
      normalPosition [i * 3 + 2] = finalNormal.z();
      finalNormal.print();
    }


  this->initBuffer(normalPosition , normalSize, Render::NORMAL_TYPE);
  delete [] normalPosition ;
  
  }else{
    unsigned normalSize = mesh.amountOfVertex() * 6;  

    float *normalPosition = new float [normalSize];
    for (unsigned i = 0; i < mesh.amountOfFace(); ++i) {
      normalPosition [mesh.face(i).idx(0)  * 3 + 0] = mesh.normal(mesh.face(i).inx(0)).x();
      normalPosition [mesh.face(i).idx(0) * 3 + 1] = mesh.normal(mesh.face(i).inx(0)).y();
      normalPosition [mesh.face(i).idx(0) * 3 + 2] = mesh.normal(mesh.face(i).inx(0)).z();
      
      normalPosition [mesh.face(i).idx(1) * 3 ] = mesh.normal(mesh.face(i).inx(1)).x();
      normalPosition [mesh.face(i).idx(1) * 3 + 1] = mesh.normal(mesh.face(i).inx(1)).y();
      normalPosition [mesh.face(i).idx(1) * 3 + 2] = mesh.normal(mesh.face(i).inx(1)).z();
      
      normalPosition [mesh.face(i).idx(2)* 3 ] = mesh.normal(mesh.face(i).inx(2)).x();
      normalPosition [mesh.face(i).idx(2) * 3 + 1] = mesh.normal(mesh.face(i).inx(2)).y();
      normalPosition [mesh.face(i).idx(2) * 3 + 2] = mesh.normal(mesh.face(i).inx(2)).z();
    }
    
    
    this->initBuffer(normalPosition , normalSize, Render::NORMAL_TYPE);
    delete [] normalPosition ;
  }
}

} //END namespace
