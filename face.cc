#include "face.h"
#include <cassert>

namespace uni {
	
Face::Face() {}

Face::Face(unsigned int idx0, unsigned int idx1, unsigned int idx2, unsigned int inx0, unsigned int inx1, unsigned int inx2) {
  _idx[0] = idx0;
  _idx[1] = idx1;
  _idx[2] = idx2;

  _inx[0] = inx0;
  _inx[1] = inx1;
  _inx[2] = inx2;
}

Face::~Face() {}

unsigned int Face::idx(unsigned int i) const {
  assert(i < 3 && "index out of bound");
  return _idx[i];
}

unsigned int Face::inx(unsigned int i) const {
  assert(i < 3 && "index out of bound");
  return _inx[i];
}
	
} //END namespace
