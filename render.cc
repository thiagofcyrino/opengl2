#include "render.h"

#include <GL/glew.h>
#include <cstring>

namespace uni {

const unsigned Render::_renderModes[3] = {GL_TRIANGLES, GL_LINE_LOOP, GL_POINTS};

Render::Render(bool useShader)
  :_useShader(useShader)
{
  _renderMode = TRIANGLE_MODE;
}

Render::~Render() {
  glDeleteBuffers(1, &_data[POSITION_TYPE]);
  glDeleteBuffers(1, &_data[NORMAL_TYPE]);
  glDeleteBuffers(1, &_indiceVB);
}

void Render::setMaterial(const Material &mat) {
  _material = mat;
}

void Render::setShader(Shader *shader) {
  _shader = shader;
  _useShader = true;
}

void Render::setUseShader(bool status) {
  _useShader = status;
}

void Render::draw() {
  _material.setMaterial();
  glPointSize(5);
  glLineWidth(5);

  if (_useShader) _shader->enable();
  this->drawVBO();
  _shader->disable();
}

void Render::initIndices(unsigned *indices, unsigned indicesSize) {
  glGenBuffers(1, &_indiceVB);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indiceVB);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize * sizeof(unsigned), indices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Render::initBuffer(float *data, unsigned dataSize, Render::DataTypeE type) {
  glGenBuffers(1, &_data[type]);
  glBindBuffer(GL_ARRAY_BUFFER, _data[type]);
  glBufferData(GL_ARRAY_BUFFER, dataSize * sizeof(float), data, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Render::drawVBO() {
  glBindBuffer(GL_ARRAY_BUFFER, _data[POSITION_TYPE]);
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, _data[NORMAL_TYPE]);
  glEnableClientState(GL_NORMAL_ARRAY);
  glNormalPointer(GL_FLOAT, 0, 0);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indiceVB);
  glEnableClientState(GL_ELEMENT_ARRAY_BUFFER);

  glDrawElements(_renderModes[_renderMode], _indicesSize, GL_UNSIGNED_INT, 0);
}

} //END namespace

