#include "sg.h"
#include "init_manager.h"
#include "loader_factory.h"

#include <iostream>

#include <GL/glew.h>
namespace uni {

SG::SG(unsigned w, unsigned h)
        :_w(w), _h(h), _done(false)
{
    this->start();
    this->init();
    this->mainLoop();
}

SG::~SG() {
    delete _render;
    delete _shader;
}

void SG::start() {
    InitManager::initSDLVideo(_w, _h);
    InitManager::initGL();
    InitManager::initView(_w, _h);
    InitManager::initGlew();
}

void SG::init() {
    _useShader = false;
    _rotation = _rX = _rY = 0;

    _mesh = LoaderFactory::createMeshFromObjFile("cavalo1.obj");

    _render = (new TriangleMeshRender(_mesh, false));
/*
    _light = new Light(Vertex(0, 0, 1), GL_LIGHT0);
    _light->setAmbient(Color(.5, .5, .5));
    _light->setDiffuse(Color(1, 1, 1));
    _light->setSpecular(Color(1, 1, 1));
    _light->enable(true);

    Material mat;
    mat.setAmbient(Color(.5, .5, .5));
    mat.setDifuse(Color(1, 1, 1));
    mat.setSpecular(Color(1, 1, 1));
    mat.setShininess(50.0);

    _render->setMaterial(mat);*/
    
    _coneAngle = 7.0;
    _coneX = 0.0;
    _coneY = 0.0;
     initShader();
        
}
void SG::initShader()
{
    _shader = new Shader(true);
    _shader->vertex(LoaderFactory::loadShaderCode("../test.vert"));
    _shader->fragment(LoaderFactory::loadShaderCode("../toonShading.frag"));
    
    modifyShader();

}
void SG::modifyShader()
{
    _shader->build();
    _shader->enable();
   
    _shader->setUniform(.7, .7, .7, "mat_kd");
    _shader->setUniform(1, 1, 1, 1, "mat_ks");
    _shader->setUniform(50.0, "mat_sh");
    
    _shader->setUniform(0, 0, 0.4, 1, "light_a");
    _shader->setUniform(0, 0, 1, 1, "light_p");
    _shader->setUniform(1, 1, 0, 1, "light_kd");
    _shader->setUniform(1, 1, 1, 1, "light_ks");
    
    _shader->setUniform(_coneAngle , "spot_cutoff");
    _shader->setUniform(_coneX, _coneY, -1,  "spot_direction");

    _render->setShader(_shader);
    _render->setUseShader(_useShader);
}



void SG::update() {

    _rotation = 0;
    if(_useShader)
      modifyShader();
}

void SG::mainLoop() {
    _tex = LoaderFactory::loadTextureImage("img.png");


    this->initCubeVBO();
    
  
    SDL_Event e;
    while (!_done) {
        this->checkEvent(e);
        this->update();
	

        this->draw();    
    }

    SDL_Quit();
}

void SG::draw() {    
    
    glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glPushMatrix();
    
    this->modify();
    
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _tex.index());
    
//     this->drawVBO(GL_QUADS, 4);
    _render->draw();    

    glPopMatrix();
    
  
    glLoadIdentity();
    glPushMatrix();

    glTranslatef(5.f, -3.f, -13.f);	
    this->drawVBO(GL_QUADS, 4);
    
    glPopMatrix();


    SDL_GL_SwapBuffers();
}

void SG::modify() {
    glTranslatef(0.f, 0.f, -16.f);
    glRotatef(_rotation, 1.f, 0.f, 0.f);
    glRotatef(_rX, 1.f, 0.f, 0.f);
    glRotatef(_rY, 0.f, 1.f, 0.f);
}

void SG::checkEvent(SDL_Event& e) {
    while (SDL_PollEvent(&e)) {
        switch (e.type) {
        case SDL_QUIT:
            _done = true;
            break;
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
            case SDLK_ESCAPE:
                _done = true;
                break;
            case SDLK_LEFT:
                _rY += 10;
                break;
            case SDLK_RIGHT:
                _rY -=  10;
                break;
            case SDLK_UP:
                _rX += 10;
                break;
            case SDLK_DOWN:
                _rX -=  10;
                break;
	    case SDLK_q:
                _coneAngle--;
                break;	
	    case SDLK_e:
                _coneAngle++;
                break;
	    case SDLK_w:
                _coneY = _coneY + 0.03;
                break;
	    case SDLK_s:
                _coneY=  _coneY - 0.03;;
                break;		
	    case SDLK_a:
                _coneX=  _coneX - 0.03;;
                break;
	    case SDLK_d:
                _coneX = _coneX + 0.03;
                break;
            case SDLK_l:
                _useShader = !_useShader;
                _render->setUseShader(_useShader);
                std::cout << "Shader:" << (_useShader? "on" : "off") << std::endl;
                break;
            default:
                break;
            }
            break;
        }
    }
}






void SG::initCubeVBO()
{
  
//     unsigned int index[] = {0,1,2,3,2,3,7,6,6,2,1,5,1,5,4,0,0,4,7,3,5,4,7,6};
  unsigned int index[] = {0,1,2,3};
    
    float position[] = {  
      1.f ,  1.f, 1.f, 
      -1.f,  1.f, 1.f,
      -1.f, -1.f, 1.f,
      1.f , -1.f, 1.f
//       1.f ,  1.f, -1.f, 
//       -1.f,  1.f, -1.f,
//       -1.f, -1.f, -1.f,
//       1.f , -1.f, -1.f 
    /*
     1.f,  1.f, 1.f,
     -1.f, 1.f, 1.f,
    -1.f,  -1.f, 1.f,
    1.f, -1.f, 1.f,*/
    };
    
    float color[] = {  
      0.f ,  0.f, 0.f, 
      0.f,  0.f, 0.f,
      0.f, 0.f, 0.f,
      0.f , 0.f, 0.f
//       0.f ,  1.f, 1.f, 
//       1.f,  0.f, 1.f,
//       1.f, 1.f, 1.f,
//       0.5 , 0.5, 0.5
      /*
      1.f ,  0.f, 0.f, 
      0.f,  0.f, 1.f,
      0.f, 1.f, 0.f,
      1.f , 1.f, 0.f*/
    };
    
    float texture[] =
    {
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	0.0f, 1.0f 
    };
    
    unsigned int sizeIndex = (sizeof(index) / sizeof(unsigned int));
    unsigned int sizePosition = (sizeof(position) / sizeof(float));
    unsigned int sizeColor = (sizeof(color) / sizeof(float));
        
    glGenBuffers(1, &_indexVBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexVBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeIndex * sizeof(unsigned int), index, GL_STATIC_DRAW);
    
    
    glGenBuffers(1, &_positionVBO);
    glBindBuffer(GL_ARRAY_BUFFER, _positionVBO);
    glBufferData(GL_ARRAY_BUFFER, sizePosition * sizeof(float), position, GL_STATIC_DRAW);
    
    
    glGenBuffers(1, &_colorVBO);
    glBindBuffer(GL_ARRAY_BUFFER, _colorVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeColor * sizeof(float), color, GL_STATIC_DRAW);
    
    glGenBuffers(1, &_textureVBO);
    glBindBuffer(GL_ARRAY_BUFFER, _textureVBO);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), texture, GL_STATIC_DRAW);


}


void SG::drawVBO(unsigned int mode, unsigned int sizeVertex) { 
    
    
    glBindBuffer(GL_ARRAY_BUFFER, _positionVBO);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3,GL_FLOAT,0,0);
    
    glBindBuffer(GL_ARRAY_BUFFER, _colorVBO);
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(3,GL_FLOAT,0,0);
        
    glBindBuffer(GL_ARRAY_BUFFER, _textureVBO);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2,GL_FLOAT,0,0);
        
    glEnableClientState(GL_ELEMENT_ARRAY_BUFFER); 
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexVBO);
        
    glDrawElements(mode, sizeVertex, GL_UNSIGNED_INT , 0);
    
    //destroy
    glDisableClientState(GL_ELEMENT_ARRAY_BUFFER);
    glDisableClientState(GL_VERTEX_ARRAY);

}




}//END namespace

