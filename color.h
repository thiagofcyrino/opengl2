#ifndef UNI_COLOR_H_
#define UNI_COLOR_H_

namespace uni {

struct Color {

  Color() {
    _r = _g = _b = 0;
    _a = 1;
  }

  Color(float r, float g, float b, float a = 1) {
    _r = r;
    _g = g;
    _b = b;
    _a = a;
  }

  float _r, _g, _b, _a;
};

} //END namespace

#endif
