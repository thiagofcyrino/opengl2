#ifndef UNI_TRIANGLE_MESH_H_
#define UNI_TRIANGLE_MESH_H_

#include "render.h"
#include "triangle_mesh.h"

namespace uni {

class TriangleMeshRender : public Render {
public:
    TriangleMeshRender(const TriangleMesh&, bool calculateNormals);
    virtual ~TriangleMeshRender();

private:
    void init(const TriangleMesh& mesh);
    void setPosition(const TriangleMesh& mesh);
    void setIndices(const TriangleMesh& mesh);
    void setNormal(const TriangleMesh& mesh);
    bool _calculateNormals;
};

} //END namespace

#endif
