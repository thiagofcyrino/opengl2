varying vec3 N;
varying vec3 v;
varying vec3 cor;
void main(){
  v = vec3(gl_ModelViewMatrix * gl_Vertex);       
  N = normalize(gl_NormalMatrix * gl_Normal);
  gl_Position = ftransform();
  cor = normalize(gl_Vertex.xyz);
}