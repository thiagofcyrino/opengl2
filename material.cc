#include "material.h"

#include <GL/glew.h>

namespace uni {

Material::Material() {}

Material::~Material() {}

void Material::setAmbient(const Color &c) {
  _a = c;
}

void Material::setDifuse(const Color &c) {
  _d = c;
}

void Material::setSpecular(const Color &c) {
  _s = c;
}

void Material::setShininess(float v) {
  _shininess = v;
}

void Material::setMaterial() {
  float aArray[] = {_a._r, _a._g, _a._b, _a._a};
  float dArray[] = {_d._r, _d._g, _d._b, _d._a};
  float sArray[] = {_d._r, _d._g, _d._b, _d._a};
  float ssArray[] = {_shininess};
  
  
  glMaterialfv(GL_FRONT,GL_AMBIENT,aArray);
  glMaterialfv(GL_FRONT,GL_DIFFUSE,dArray);
  glMaterialfv(GL_FRONT,GL_SPECULAR,sArray);
  
  glMaterialfv(GL_FRONT,GL_SHININESS,ssArray);
  
  
  

    //informacoes de material aqui
}

} //END namespace
